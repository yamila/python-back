FROM python:latest
MAINTAINER Yamila Moreno yamila.moreno@kaleidos.net
WORKDIR /

# Install requirements
COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY src /mymac
WORKDIR /mymac
