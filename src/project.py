from flask import Flask
import socket
from uuid import getnode

app = Flask(__name__)


@app.route("/")
def hello():
    hostname = socket.gethostname()
    mac = getnode()
    return "<h1 style='color:blue'>Hello, my hostname is {}</h1>".format(hostname)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
